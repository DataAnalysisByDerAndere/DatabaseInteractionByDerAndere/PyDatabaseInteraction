"""
@title: pymysql-demo1
@author: DerAndere
@created: 2018
Copyright 2018 - 2022 DerAndere
@license: MIT
@language: Python 3.6
@about: Interaction between MySQL database and Python script using PyMySQL as database driver. 
"""

import PyMySQL
dbconnection = PyMySQL.connect(host = "localhost", user = "AdminLocal",
                  passwd = "<AdminLocalUserPassword>", db = "Database1")

cursor = dbconnection.cursor()
cursor.execute("SELECT cu, cv FROM tx WHERE <condition(s)>")
data = cursor.fetchall() 
print(data)
cursor.close()
dbconnection.close()
