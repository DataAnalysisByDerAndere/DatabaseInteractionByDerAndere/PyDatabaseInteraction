"""
@title: DataAnalystByDerAndere/py_database_interaction_pack1
@author: DerAndere
@created: 2018
SPDX-License-Identifier: MIT
Copyright 2018 - 2022 DerAndere
@license: MIT
@about: PyDatabaseInteraction package1
"""
__author__      = "DerAndere"
__date__        = 2018
__copyright__   = "Copyright (c) 2018 - 2022 DerAndere"
__license__     = "MIT"
__version__     = "0.0.1"
__status__      = "Prototype"
__maintainer__  = "DerAndere"
__contact__     = "DerAndere"

