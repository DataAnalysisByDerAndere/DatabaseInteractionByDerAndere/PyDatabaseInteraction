"""
@title: sqlalchemy-pymysql-demo1
@author: DerAndere
@created: 2018
SPDX-License-Identifier: MIT
Copyright 2018 - 2022 DerAndere
@license: MIT
@language: Python 3.6
@about: Interaction between MySQL database and Python script using SQLAlchemy with PyMySQL as database driver and MySQL as the SQL dialect. see https://docs.sqlalchemy.org/en/14/orm/extensions/declarative/#sqlalchemy.ext.declarative.DeferredReflection."""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy.ext.declarative import DeferredReflection

# Using declarative base with deferred reflection

engine1 = create_engine("mysql+pymysql://AdminLocal:<password>@localhost: 3306/Database1?charset=utf8mb4")

Base1 = declarative_base(engine1)

# Create python object to store the table that gets fetched from the database
# later
# See https://docs.sqlalchemy.org/en/14/orm/declarative_tables.html
class ReflectiveBase1(DeferredReflection, Base1):
    """This is an abstract base for deferred reflection with multiple 
    engines with individual prepare()"""
    __abstract__ = True


# reflect several tables from the database using deferred reflection:
# See http://danielweitzenfeld.github.io/passtheroc/blog/2014/10/12/datasci-sqlalchemy/
# See https://docs.sqlalchemy.org/en/14/orm/declarative_tables.html
# See https://docs.sqlalchemy.org/en/14/core/metadata.html?highlight=autoload_with#sqlalchemy.schema.Table.params.autoload
class tblx(ReflectiveBase1):
    """Description of tblx"""
    __tablename__ = "tx"
    __table_args__ = {'autoload_with': engine1}

ReflectiveBase1.prepare(engine1)

# define the loadSession() function
def loadSession():
    """
    loadSession() stores metadata in the variable 
    metadata and returns sessionmaker(bind=engine)() which is the 
    representation of the database
    """
    metadata1 = Base1.metadata
    Session = sessionmaker(bind=engine1)
    session = Session()  # together with last line equal to session = sessionmaler(bind=engine1)()
    return session
 
# when running, it creates a variable session1 which represents the database. 
# this representation of the database gets queried and the result is saved 
# as the variable data. 
if __name__ == "__main__":
    session1 = loadSession()
    data = session1.query(tblx, session = session1).options(load_only("cu", "cv")).filter_by(<condition(s)>).all()  # query the database. Equivalent to Query([tblx], session=some_session...
    # print the data that was fetched and saved in the variable data to get the 
    # data output in the console.
    print data
    print data.cu
    print data.cv

"""
n = 10000
# batch insert or update via the "bulk" API of the SQLAlchemy ORM, using a 
# dictionary. Replace insert() by update() if required.
    session1.bulk_insert_mappings(tblx, [
    # create a list ([] is a list) of dictionaries ({} is a dictionary) using 
    # list comprehension
        {"cu": "entry %d" % rowindex,
        "cv": "val %d" % rowindex} 
        for rowindex in range(n)    # list comprehension.
                                    # In python 3, range() replaces xrange()
    ])
    session1.commit()

# batch insert or update using SQLAlchemy core: Replace insert() by update() 
# if required. Sessions have to be closed (session.close()) so no transactions 
# are active.
# See https://tutorials.technology/tutorials/Fast-bulk-insert-with-sqlalchemy.html
with engine.begin() as connection1:
    connection1.execute(tblx.__table__.insert(), [  
    # create a list ([] is a list) of dictionaries ({} is a dictionary) using 
    # list comprehension
        {"cu": "entry %d" % rowindex,
        "cv": "val %d" % rowindex}
        for rowindex in range(n)    # list comprehension.
                                    # In Python 3, range() replaces xrange()
    ]) 
"""


"""
# using automap
# see https://books.google.de/books?id=5p0bCwAAQBAJ&pg=PA133&lpg=PA133&dq=Automap+SQLAlchemy&source=bl&ots=u5YjHPV6-G&sig=LRAuYnsoR5pMQLiin4SUu7VlQa8&hl=de&sa=X&ved=2ahUKEwjJtdXFpMLdAhWFxosKHYcNBoA4ChDoATACegQICRAB#v=onepage&q=Automap%20SQLAlchemy&f=false
# see http://docs.sqlalchemy.org/en/latest/orm/extensions/automap.html

from sqlalchemy import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker

# The declarative base used for the SQLAlchemy reflection.
Base1 = automap_base()

def main():
    engine1 = create_engine("mysql+pymysql://AdminLocal:<password>@localhost: 3306/Database1?charset=utf8mb4")

    # Perform automap and create a session
    Base1.prepare(engine1, reflect=True)    # reflects the database, creates 
                                            # table objects of identifier matching
                                            # the tablename in the database
                                            # and respective classes having
                                            # the same identifier, but capitalized 
                                            # as well as 
                                            # <Table>_<related_object>_collection 
                                            # for tables related by foreign keys  
    Tx = Base1.classes.tx    # should match tablename in the database.
                             # Tx.ty_collection now exists and is equal to 
                             # Base1.classes.tx.ty_collection
    Ty = Base1.classes.ty 
    
    Session1 = sessionmaker(bind=engine1)
    session1 = Session1()
    # Use the session1 and the <related_object>_collection object created by automap_base()
    data1 = session1.query(Tx).filter_by(<condition>).all()    # Now, data1.ty_collection 
                                                               # exists, a subset of 
                                                               # Tx.ty_collection 
                                                               # (which equals 
                                                               # Base1.classes.Ty_collection).
    for ty in data1.ty_collection:  # data.ty_collection is subset of Tx.ty_collection
        print(data1.cv, ty.cv)


if __name__ == "__main__":
    main()
"""

"""
# Using automap with predefined MetaData
# see https://jeffknupp.com/blog/2015/07/12/flask-and-sqlalchemy-magic/ 
# see http://docs.sqlalchemy.org/en/latest/orm/extensions/automap.html#generating-mappings-from-an-existing-metadata
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine1 = create_engine("mysql+pymysql://AdminLocal:<password>@localhost: 3306/Database1?charset=utf8mb4")

metadata1 = MetaData() # optional argument bind=engine1

metadata1.reflect(bind=engine1, only=["tx", "ty"])  # only reflect specified tables
Base1 = automap_base(metadata=metadata1)    # when Model1 = 
                                            # declarative_base(metadata=metadata1) 
                                            # was defined before, the argument 
                                            # declearative_base=Model1 can be added
Base1.prepare(engine1, reflect=True)

    Tx = Base1.classes.tx    # should match tablename in the database.
                             # Tx.ty_collection now exists and is equal to 
                             # Base1.classes.tx.ty_collection
    Ty = Base1.classes.ty 

Session1 = sessionmaker(metadata=metadata1, bind=engine1)
session1 = Session1()

# Use the session1 and the <related_object>_collection object created by automap_base()
data1 = session1.query(Tx).filter_by(<condition>).all()    # Now, data1.ty_collection 
                                                           # exists, a subset of 
                                                           # Tx.ty_collection 
                                                           # (which equals 
                                                           # Base1.classes.Ty_collection).
for ty in data1.ty_collection:  # data.ty_collection is subset of Tx.ty_collection
    print(data1.cv, ty.cv)

"""


