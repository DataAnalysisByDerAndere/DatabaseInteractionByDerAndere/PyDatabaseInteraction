'''
@title: sqlalchemy-demo1
@author: DerAndere
@created: 2018
Copyright 2018 - 2022 DerAndere
@license: MIT
@language: Python 3.6
@about: Interaction between MySQL database and Python script using the SQL Expression Language of SQLAlchemy with PyMySQL as database driver and MySQL as the SQL dialect. .
'''

from sqlalchemy import create_engine

engine = create_engine("mysql+pymysql://AdminLocal:<password>@localhost: 3306/Database1?charset=utf8mb4")

conDatabase1 = engine.connect()


from sqlalchemy.sql import select

result = conDatabase1.execute(select([tx.c.cu, tx.c.cv]).where(tx.c.cw >= 5))

data = result.fetchall  # result as tuple
for row in result
    ca[row] = row[tx.c.cu] # equal to row[1] , equal to row['cu '] 
    cb[row] = row[tx.c.cv] # equal to row[2] , equal to row['cv']
print(data)
print(ca)
print(cb)

result.close()
